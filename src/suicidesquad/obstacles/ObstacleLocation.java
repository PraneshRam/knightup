/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package suicidesquad.obstacles;

import suicidesquad.obstacles.Constants.ObstacleTypes;

/**
 * @author siva1
 */
public class ObstacleLocation {
    private int floor, room;
    private ObstacleTypes type;

    public ObstacleLocation( int floor,int room, ObstacleTypes type) {
        this.floor = floor;
        this.room = room;
        this.type = type;
    }

    public int getRoom() {
        return this.room;
    }

    public int getFloor() {
        return this.floor;
    }

    public ObstacleTypes getObstacle() {
        return this.type;
    }
}
