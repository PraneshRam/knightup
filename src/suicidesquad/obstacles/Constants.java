/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package suicidesquad.obstacles;

/**
 * @author siva1
 */
public class Constants {
    static public int Rows = 10;
    static public int Columns = 10;

    public enum ObstacleTypes {
        NONE, HEROBOT, RADAR, MISSILE, TANK, ENEMY_COMMANDER;
    }
}
