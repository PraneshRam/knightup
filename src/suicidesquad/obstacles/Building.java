/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package suicidesquad.obstacles;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import suicidesquad.obstacles.Constants.ObstacleTypes;

/**
 * @author siva1
 */
public class Building {
    Constants.ObstacleTypes[][] buildingmap;
    public JSONArray ja;
    static public JSONObject js;

    public Building() {
        buildingmap = new Constants.ObstacleTypes[Constants.Rows][Constants.Columns];
    }

    public Building(ObstacleLocation locations[]) {
        js = new JSONObject();
        ja = new JSONArray();
        buildingmap = new Constants.ObstacleTypes[Constants.Rows][Constants.Columns];
        for (int i = 0; i < 5; i++) {
            JSONObject obs = new JSONObject();

            setObstacletype(locations[i]);
            switch (locations[i].getObstacle()) {
                case ENEMY_COMMANDER:
                    obs.put("type", Constants.ObstacleTypes.ENEMY_COMMANDER.toString());
                    break;
                case HEROBOT:
                    obs.put("type", Constants.ObstacleTypes.HEROBOT.toString());
                    break;
                case MISSILE:
                    obs.put("type", Constants.ObstacleTypes.MISSILE.toString());
                    break;
                case RADAR:
                    obs.put("type", Constants.ObstacleTypes.RADAR.toString());
                    break;
                case TANK:
                    obs.put("type", Constants.ObstacleTypes.TANK.toString());
                    break;
            }
            obs.put("row", locations[i].getFloor());
            obs.put("column", locations[i].getRoom());
            ja.add(obs);
            obs = null;
        }
        js.put("obstacles", ja);
    }

    public void setObstacletype(ObstacleLocation location) {
        buildingmap[location.getFloor()][location.getRoom()] = location.getObstacle();
    }

    public Constants.ObstacleTypes getObstacletype(int floor, int room) {
        return this.buildingmap[floor][room];

    }

    public boolean isValidmove(Move currentPosition, Move nextPosition) {

        if (currentPosition.getRow() < Constants.Rows && nextPosition.getRow() < Constants.Rows &&
            currentPosition.getColumn() < Constants.Columns && nextPosition.getColumn() < Constants.Columns) {
            int rowDiff = Math.abs(currentPosition.getRow() - nextPosition.getRow());
            int colDiff = Math.abs(currentPosition.getColumn() - nextPosition.getColumn());

            if ((rowDiff == 1 && colDiff == 2) || (rowDiff == 2 && colDiff == 1))
                return true;
        }
        return false;

    }

}
