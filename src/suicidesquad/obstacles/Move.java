/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package suicidesquad.obstacles;

/**
 * @author siva1
 */
public class Move {
    private int floor;
    private int room;
    

    public Move() {

    }

    public Move(int floor,int room) {
        this.floor = floor;
        this.room = room;        
    }

    public int getColumn() {
        return this.room;
    }

    public int getRow() {
        return this.floor;
    }

    public void setColumn(int room) {
        this.room = room;
    }

    public void setRow(int floor) {
        this.floor = floor;
    }
}
