/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import suicidesquad.obstacles.Bot;
import suicidesquad.obstacles.Building;
import suicidesquad.obstacles.Move;

import java.util.ArrayList;
import java.util.List;

/**
 * @author siva1
 */
public class UserBot implements Bot {


    @Override
    public List makeMoves(Building BuildingPlan) {
        List<Move> moves = new ArrayList<Move>();
        Move move[] = new Move[4];
        move[0] = new Move(6, 7);
        move[1] = new Move(5, 5);
        move[2] = new Move(4, 3);
        move[3] = new Move(2, 2);
        for (int i = 0; i < 4; i++) {
            moves.add(move[i]);
        }
        return moves;
    }
}
