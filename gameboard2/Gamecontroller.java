/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import suicidesquad.obstacles.Building;
import suicidesquad.obstacles.Constants;
import suicidesquad.obstacles.Constants.ObstacleTypes;
import suicidesquad.obstacles.Move;
import suicidesquad.obstacles.ObstacleLocation;

import java.util.Arrays;
import java.util.List;

/**
 * @author siva1
 */
public class Gamecontroller {
    private UserBot user;
    private Building buildingmap;
    public JSONArray ja;

    //returns ObstacleLocations from one of the Question Sets
    public Gamecontroller(UserBot user) {
        this.user = user;
        initializeBuilding();
        ja = new JSONArray();
    }

    private void initializeBuilding() {
        ObstacleLocation locations[] = loadObstaclePosition();
        buildingmap = new Building(locations);
    }

    private ObstacleLocation[] loadObstaclePosition() {
        ObstacleLocation location[] = new ObstacleLocation[5];
        location[0] = new ObstacleLocation(3, 2, ObstacleTypes.ENEMY_COMMANDER);
        location[1] = new ObstacleLocation(4, 7, ObstacleTypes.TANK);
        location[2] = new ObstacleLocation(7, 0, ObstacleTypes.MISSILE);
        location[3] = new ObstacleLocation(8, 8, ObstacleTypes.HEROBOT);
        location[4] = new ObstacleLocation(0, 3, ObstacleTypes.RADAR);
        return location;
    }

    boolean simulate() {
        //user=new UserBot();
        // BuildingMap=new Building();


        int[][] path = new int[10][10];
        for (int m = 0; m < 10; m++) {
            Arrays.fill(path[m], 0);
        }
        ObstacleLocation orig = new ObstacleLocation(-1, -1, ObstacleTypes.NONE);
        ObstacleLocation dest = new ObstacleLocation(-1, -1, ObstacleTypes.NONE);
        ;
        ObstacleLocation location;

        List<Move> moves = user.makeMoves(buildingmap);
        for (int i = 0; i < Constants.Rows; i++) {
            for (int j = 0; j < Constants.Columns; j++) {

                if (buildingmap.getObstacletype(i, j) == ObstacleTypes.HEROBOT) {
                    location = new ObstacleLocation(i, j, ObstacleTypes.HEROBOT);
                    orig = location;

                } else if (buildingmap.getObstacletype(i, j) == ObstacleTypes.MISSILE) {
                    location = new ObstacleLocation(i, j, ObstacleTypes.MISSILE);
                    markPos(path, location);
                } else if (buildingmap.getObstacletype(i, j) == ObstacleTypes.RADAR) {
                    location = new ObstacleLocation(i, j, ObstacleTypes.RADAR);
                    markPos(path, location);
                } else if (buildingmap.getObstacletype(i, j) == ObstacleTypes.TANK) {
                    location = new ObstacleLocation(i, j, ObstacleTypes.TANK);
                    markPos(path, location);
                } else if (buildingmap.getObstacletype(i, j) == ObstacleTypes.ENEMY_COMMANDER) {
                    location = new ObstacleLocation(i, j, ObstacleTypes.ENEMY_COMMANDER);
                    dest = location;
                }


            }
        }
        Move prevmove = new Move(orig.getFloor(), orig.getRoom());
        int i;
        for (i = 0; i < moves.size(); i++) {
            JSONObject mov = new JSONObject();


            if (!((moves.get(i).getRow() == dest.getFloor()) && moves.get(i).getColumn() == dest.getRoom())) {
                if (buildingmap.isValidmove(prevmove, moves.get(i)) && (moves.get(i).getRow() != -1 && moves.get(i).getColumn() != -1)) {
                    mov.put("row", moves.get(i).getRow());
                    mov.put("column", moves.get(i).getColumn());
                    //System.out.println(moves[i].GetRow());
                    ja.add(mov);
                    mov = null;
                    prevmove = moves.get(i);
                    continue;
                } else {
                    if (!buildingmap.isValidmove(prevmove, moves.get(i))) {
                        buildingmap.js.put("moves", ja);
                        //mov=null;
                        buildingmap.js.put("IsGameValid", "false");
                        print();
                    } else if ((moves.get(i).getRow() != -1 && moves.get(i).getColumn() != -1)) {
                        buildingmap.js.put("IsGameValid", "true");
                        buildingmap.js.put("GameStatus", "lose");
                        print();
                    }
                    return false;
                }

            }
            if (buildingmap.isValidmove(prevmove, moves.get(i))) {
                mov.put("row", moves.get(i).getRow());
                mov.put("column", moves.get(i).getColumn());
                ja.add(mov);
                mov = null;
                buildingmap.js.put("moves", ja);
                buildingmap.js.put("IsGameValid", "true");
                buildingmap.js.put("GameStatus", "win");
                print();
                return true;
            } else {
                buildingmap.js.put("moves", ja);
                //mov=null;
                buildingmap.js.put("IsGameValid", "false");
                print();
                return false;
            }
        }
        if (!((moves.get(i-1).getRow() == dest.getFloor()) && moves.get(i-1).getColumn() == dest.getRoom())) {
            buildingmap.js.put("moves", ja);
            //mov=null;
            buildingmap.js.put("IsGameValid", "false");
            print();
        }
        return false;
    }

    void markPos(int[][] path, ObstacleLocation Location) {

        switch (Location.getObstacle()) {
            case RADAR: {
                markRow(path, Location.getFloor());
                markCol(path, Location.getRoom());
                markDiag(path, Location.getFloor(), Location.getRoom());
            }
            break;
            case MISSILE: {
                markDiag(path, Location.getFloor(), Location.getRoom());
            }
            break;
            case TANK: {

                markRow(path, Location.getFloor());
                markCol(path, Location.getRoom());
            }
        }

    }

    void markRow(int[][] path, int row) {

        for (int j = 0; j < Constants.Columns; j++)
            path[row][j] = -1;
    }

    void markCol(int[][] path, int col) {

        for (int i = 0; i < Constants.Columns; i++)
            path[i][col] = -1;
    }

    void markDiag(int[][] path, int row, int col) {
        int colInc = col + 1;
        int rowInc = row;


        for (int i = row - 1; i >= 0 && colInc < Constants.Columns; i--) {
            path[i][colInc++] = -1;
        }

        colInc = col - 1;
        for (int i = row - 1; i >= 0 && colInc >= 0; i--)
            path[i][colInc--] = -1;

        colInc = col + 1;
        for (int i = row + 1; i < Constants.Rows && colInc < Constants.Columns; i++)
            path[i][colInc++] = -1;

        colInc = col - 1;
        for (int i = row + 1; i < Constants.Rows && colInc >= 0; i++)
            path[i][colInc--] = -1;

    }

    void print() {
        //System.out.println("gg");
        System.out.println(buildingmap.js);
    }
}
